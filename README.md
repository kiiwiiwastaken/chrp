# CHRP

A third-party repo for Solus that has packages not available in Solus. Not supported *and proud* ;)

# Setup

Run the following command:

```sudo eopkg add-repo chrp https://gitlab.com/kiiwiiwastaken/chrp/raw/master/packages/eopkg-index.xml.xz```

# Package List

- [RGBDS](https://rgbds.gbdev.io/): An assembly toolchain for the Nintendo Game Boy

- [cgo](https://github.com/kieselsteini/cgo): A terminal based gopher client

# Can I trust you????

This repo has all the source available so you can read through and make sure.